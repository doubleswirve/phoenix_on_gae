# PhoenixOnGae

To start your Phoenix app:

  * Install dependencies with `mix deps.get`
  * Install Node.js dependencies with `npm install`
  * Start Phoenix endpoint with `mix phoenix.server`

Now you can visit [`localhost:4000`](http://localhost:4000) from your browser.

## Init Google Project

```shell
gcloud init
```

## Deploy to Google App Engine

```shell
./bin/deploy.sh
```

## Todo

  * Cluster
  * ENV vars outside repo
