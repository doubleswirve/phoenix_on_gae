#!/bin/bash
set -e

mix docker.build
mix docker.release

cp Dockerfile.release Dockerfile

sudo gcloud app deploy --verbosity=debug

rm Dockerfile
