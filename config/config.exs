# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

# Configures the endpoint
config :phoenix_on_gae, PhoenixOnGae.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "nISus4jH9xNFq+p3D4Nr0utKd0nvqEgbtIeB+MKCUy7FlidjS4pfSguQWTv9FVgX",
  render_errors: [view: PhoenixOnGae.ErrorView, accepts: ~w(html json)],
  pubsub: [name: PhoenixOnGae.PubSub,
           adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Configure mix docker dep
config :mix_docker, image: "phoenix_on_gae"

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env}.exs"
